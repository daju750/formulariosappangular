import { Component, ViewChild } from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styleUrls: ['./basicos.component.css']
})

export class BasicosComponent {

  @ViewChild('miFormularioBasico') miFormularioBasico!: NgForm;

  initForm = {
    producto : 'RTX 4080ti',
    precio: 10,
    existencias: 10
  }

  guardar(){
    this.miFormularioBasico.resetForm({
      precio: 0,
      existencias: 0
    });
  }

  nombreValid(): boolean{
    return this.miFormularioBasico?.controls.producto?.invalid && this.miFormularioBasico?.controls.producto?.touched
  }

  precioValid(): boolean{
    return this.miFormularioBasico?.controls.precio?.value<0 && this.miFormularioBasico?.controls.precio?.touched;
  }

}
