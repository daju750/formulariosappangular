import { Component } from '@angular/core';

import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-dinamicos',
  templateUrl: './dinamicos.component.html',
  styleUrls: ['./dinamicos.component.css']
})
export class DinamicosComponent {

  miFormularioDinamico: FormGroup = this.fb.group({
    nombre: ['',[Validators.required,Validators.minLength(3)]],
    favoritos: this.fb.array([
      ['Metal Gear',Validators.required],
      ['Death Strading',Validators.required]
    ],Validators.required)
  });

  nuevoFavorito: FormControl = this.fb.control('',Validators.required);

  get favoritoArr(){
    return this.miFormularioDinamico.get('favoritos') as FormArray;
  }

  campoInvalido(campo:string){
    return this.miFormularioDinamico.controls[campo].errors && this.miFormularioDinamico.controls[campo].touched;
  }

  agregarFavorito(){
    if(this.nuevoFavorito.invalid){return;}
    //this.favoritoArr.push(new FormControl(this.nuevoFavorito.value,Validators.required))
    this.favoritoArr.push(this.fb.control(this.nuevoFavorito.value,Validators.required))
    this.nuevoFavorito.reset();
   
  }

  borrar(i : number){
    this.favoritoArr.removeAt(i);
  }

  guardar(){
    if(this.miFormularioDinamico.invalid){
      this.miFormularioDinamico.markAllAsTouched();
      return;
    }
  }

  constructor(private fb:FormBuilder){}

}
