import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styleUrls: ['./basicos.component.css']
})


export class BasicosComponent implements OnInit{
/*
  miFormularioBasico: FormGroup = new FormGroup({
    nombre: new FormControl('RTX 4080ti'),
    precio: new FormControl(1500),
    existencias: new FormControl(5)
  })
*/
  constructor(private fb:FormBuilder){}

  ngOnInit(): void {
    this.miFormularioBasico.reset({
      producto: '',precio:0,existencias:0
    })
  }

  miFormularioBasico: FormGroup = this.fb.group({
    producto: ['',[Validators.required,Validators.minLength(3)]],
    precio: [,[Validators.required,Validators.min(0)]],
    existencias: [,[Validators.required,Validators.min(0)]]
  })

  campoInvalido(campo:string){
    return this.miFormularioBasico.controls[campo].errors && this.miFormularioBasico.controls[campo].touched;
  }

  guardar(){
    if(this.miFormularioBasico.invalid){
      this.miFormularioBasico.markAllAsTouched();
      return;
    }

    this.miFormularioBasico.reset();
  }

}
